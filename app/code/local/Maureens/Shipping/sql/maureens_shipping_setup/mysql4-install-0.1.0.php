<?php
$installer = $this;
$installer->startSetup();
$installer->run("
    CREATE TABLE IF NOT EXISTS {$this->getTable('maureens_shipping')} (
      `id` int(11) unsigned NOT NULL auto_increment,
      `order_id` int(11) NOT NULL,
      `shipping_rate` varchar(255) NOT NULL default '',
      `shipping_address` varchar(255) NOT NULL default '',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();