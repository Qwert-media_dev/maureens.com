<?php
class Maureens_Shipping_Block_Config_RegionList
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    protected $_itemRenderer;
    protected $_itemRenderer1;

    public function _prepareToRender()
    {
        $this->addColumn('region_label', array(
            'label' => Mage::helper('maureens_shipping')->__('Region Label'),
            'style' => 'width:100px',
        ));
        $this->addColumn('region_options', array(
            'label' => Mage::helper('maureens_shipping')->__('Region\'s Radio Options'),
            'renderer' => $this->_getRenderer()
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('maureens_shipping')->__('Add region');
    }
    protected function  _getRenderer()
    {
        if (!$this->_itemRenderer) {
            $this->_itemRenderer = $this->getLayout()->createBlock(
                'maureens_shipping/config_adminhtml_form_field_regionOptions', '',
                array('is_render_to_js_template' => true, 'element' => $this->getElement())
            );
        }
        return $this->_itemRenderer;
    }
}