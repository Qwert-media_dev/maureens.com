<?php
class Maureens_Shipping_Block_Config_Adminhtml_Form_Field_RegionOptions
    extends Mage_Core_Block_Abstract
{
    public function _toHtml()
    {
       $html = '<textarea style="height:5em;" name="'. $this->getInputName() .'" rows="3" cols="30">#{region_options}</textarea>';

        return $html;
    }

}