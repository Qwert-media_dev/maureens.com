<?php

class Maureens_Shipping_Block_Shipping_ModernCoast2 extends Mage_Checkout_Block_Onepage_Shipping_Method_Available
{
    protected $_rate;

    public function __construct()
    {
        $this->setTemplate('maureens/shipping/available/modern_coast_2.phtml');
    }

    public function getTitle()
    {
        return Mage::getStoreConfig('carriers/' . $this->_rate->getCarrier() . '/title');
    }

    /**
     * @param $rate
     */
    public function setRate($rate)
    {
        if (!$this->_rate) {
            $this->_rate = $rate;
        }
        parent::setRate($rate);
    }

    public function getRegionList()
    {
        $regionOptions = Mage::getStoreConfig('carriers/' . $this->_rate->getCarrier() . '/region_options');

        if ($regionOptions) {
            $regionOptions = explode(PHP_EOL, $regionOptions);
            return $regionOptions;
        }

    }

    public function showNotInList()
    {
        return Mage::getStoreConfig('carriers/' . $this->_rate->getCarrier() . '/show_not_in_list');

    }

}