<?php

class Maureens_Shipping_Block_Shipping_Example3 extends Mage_Checkout_Block_Onepage_Shipping_Method_Available
{
    protected $_rate;

    public function __construct()
    {
        $this->setTemplate('maureens/shipping/available/example3.phtml');
    }

    public function getTitle()
    {
        return Mage::getStoreConfig('carriers/' . $this->_rate->getCarrier() . '/title');
    }

    /**
     * @param $rate
     */
    public function setRate($rate)
    {
        if (!$this->_rate) {
            $this->_rate = $rate;
        }
        parent::setRate($rate);
    }

    public function getRegionList()
    {
        $regionList = Mage::getStoreConfig('carriers/' . $this->_rate->getCarrier() . '/dropdown_region_list');
        if ($regionList) {
            $regionList = unserialize($regionList);
            if (is_array($regionList)) {
                foreach($regionList as &$regionListRow) {
                    $regionListRow['region_options'] = explode(PHP_EOL, $regionListRow['region_options']);
                    $regionListRow['region_options'] = array_filter($regionListRow['region_options']);
                }
            } else {
                // handle unserializing errors here
            }
            return $regionList;
        }
    }

    public function getLetterList()
    {
        $letterList = Mage::getStoreConfig('carriers/' . $this->_rate->getCarrier() . '/initial_region_list');
        if ($letterList) {
            $letterList = unserialize($letterList);
            if (is_array($letterList)) {
                foreach($letterList as &$letterListRow) {
                    $letterListRow['region_options'] = explode(PHP_EOL, $letterListRow['region_options']);
                    $letterListRow['region_options'] = array_filter($letterListRow['region_options']);
                }
            } else {
                // handle unserializing errors here
            }
            return $letterList;
        }
    }

    public function showNotInList()
    {
        return Mage::getStoreConfig('carriers/' . $this->_rate->getCarrier() . '/show_not_in_list');

    }

}