<?php

class Maureens_Shipping_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getCurrentShippingMethodCode($quote)
    {
        $quoteShippingAddress = $quote->getShippingAddress();

        if (is_null($quoteShippingAddress->getId())) {
            return false;
        }
        $shippingDescription = $quote->getShippingAddress()->getShippingMethod();

        try {
            $quoteShippingAddress->collectShippingRates()->save();
            $groupedRates = $quoteShippingAddress->getGroupedAllShippingRates();

            foreach ($groupedRates as $carrierCode => $rates) {
                if (!is_null(Mage::getStoreConfig('carriers/' . $carrierCode . '/title'))) {
                    if (strpos($shippingDescription, $carrierCode) !== false) {
                        return $carrierCode;
                    }
                }

            }
        } catch (Mage_Core_Exception $e) {
            Mage::log($e->getMessage(), Zend_Log::ERR);
        }
        return false;
    }

    /**
     * @param $orderId
     * @param $quote
     * @param $selectedOption
     * @param $optionValue
     * @return bool
     */
    public function sendManualEmail($orderId, $quote, $selectedOption, $optionValue)
    {
        $carrierCode = $this->getCurrentShippingMethodCode($quote);
        if (Mage::getStoreConfig('carriers/' . $carrierCode . '/send_email') &&
            ($email = Mage::getStoreConfig('carriers/' . $carrierCode . '/manual_email'))
        ) {
            $emailTemplate = Mage::getModel('core/email_template')
                ->loadDefault('maureens_manual_email_template');
            $emailTemplate->setSenderName((Mage::getStoreConfig('trans_email/ident_general/name')?Mage::getStoreConfig('trans_email/ident_general/name'):'Owner'));
            $emailTemplate->setSenderEmail((Mage::getStoreConfig('trans_email/ident_general/email')?Mage::getStoreConfig('trans_email/ident_general/email'):'owner@example.com'));
            $emailTemplateVariables = array();
            $emailTemplateVariables['order'] = $orderId;
            $emailTemplateVariables['shipping'] = Mage::getStoreConfig('carriers/' . $carrierCode . '/title');
            $emailTemplateVariables['destination'] = $selectedOption . ' - ' . $optionValue;
            try {
                $emailTemplate->send($email, Mage::getStoreConfig('trans_email/ident_support/name'), $emailTemplateVariables);

            } catch (Exception $error) {
                Mage::getSingleton('core/session')->addError($error->getMessage());
                return false;
            }
        }
    }


}