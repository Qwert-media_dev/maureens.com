<?php

class Maureens_Shipping_Model_Observer extends Varien_Object
{
    public function saveShippingMethod($evt)
    {
        $request = $evt->getRequest();
        $quote = $evt->getQuote();
        $selectedMethod = $request->getParam('shipping_method');
        $maureensShipping = $request->getParam('maureens', false);
        $quote_id = $quote->getId();
        $data = array(
            $quote_id => array(
                $selectedMethod => $maureensShipping[$selectedMethod]
            ));
        if ($maureensShipping) {
            Mage::getSingleton('checkout/session')->setMaureensShipping($data);
        }
    }

    /**
     * @param $evt
     */
    public function saveOrderAfter($evt)
    {
        $order = $evt->getOrder();
        $quote = $evt->getQuote();
        $shippingMethod = $quote->getShippingAddress()->getShippingMethod();
        $quote_id = $quote->getId();
        $maureensShipping = Mage::getSingleton('checkout/session')->getMaureensShipping();
        if (isset($maureensShipping[$quote_id])) {

            $data['shipping_rate'] = $shippingMethod;
            /** if not-in-list is selected and textarea exists we need to send email and prepare the data*/
            if (count($maureensShipping[$quote_id][$shippingMethod]) > 1 &&
                array_key_exists('not-in-list', $maureensShipping[$quote_id][$shippingMethod])
            ) {
                /** @param $selectedOption - value in dropdown(region list) */
                $selectedOption = key(array_slice($maureensShipping[$quote_id][$shippingMethod], 0, 1));
                $optionValue = array_slice($maureensShipping[$quote_id][$shippingMethod], 1, 1);
                $optionValue = array_shift($optionValue);
                Mage::helper('maureens_shipping')->sendManualEmail($order->getIncrementId(), $quote, $selectedOption, $optionValue);
            } elseif (count($maureensShipping[$quote_id][$shippingMethod]) > 1 &&
                array_key_exists('letters', $maureensShipping[$quote_id][$shippingMethod])
            ) {
                /** @param $selectedOption - value in dropdown(letters list) */
                /** getting key value of the second element - selected letter*/
                $selectedOption = key(array_slice($maureensShipping[$quote_id][$shippingMethod], 1, 1));
                $optionValue = array_slice($maureensShipping[$quote_id][$shippingMethod], 1, 1);
                /** getting region of selected letter*/
                $optionValue = array_shift($optionValue);
            } else {
                $selectedOption = key(array_slice($maureensShipping[$quote_id][$shippingMethod], 0, 1));
                $optionValue = array_slice($maureensShipping[$quote_id][$shippingMethod], 0, 1);
                /** getting region of selected value*/
                $optionValue = array_shift($optionValue);
            }
            $data['shipping_address'] = $selectedOption . ' : ' . $optionValue;
            $data['order_id'] = $order->getId();
            $maureensShippingModel = Mage::getModel('maureens_shipping/address');
            $maureensShippingModel->setData($data);
            $maureensShippingModel->save();
        }
    }

    public function loadOrderAfter($evt)
    {
        $order = $evt->getOrder();
        if ($order->getId()) {
            $order_id = $order->getId();
            $maureensShippingCollection = Mage::getModel('maureens_shipping/address')->getCollection();
            $maureensShippingCollection->addFieldToFilter('order_id', $order_id);
            $maureensShipping = $maureensShippingCollection->getFirstItem();
            $order->setMaureenshippingObject($maureensShipping);
//            $order->save();
        }
    }

    public function loadQuoteAfter($evt)
    {
        $quote = $evt->getQuote();
        if ($quote->getId()) {
            $quote_id = $quote->getId();
            $maureensShipping = Mage::getSingleton('checkout/session')->getMaureensShipping();
            if (isset($maureensShipping[$quote_id])) {
                $data = $maureensShipping[$quote_id];
                $quote->setWebshippingData($data);
            }
        }
    }

}