<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 25.03.2016
 * Time: 15:46
 */

class Maureens_Shipping_Model_Carrier_PostaKenya extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface {
    protected $_code = 'maureens_shipping_postakenya';

    public function getFormBlock(){
        return 'maureens_shipping/shipping_example3';
    }

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $result = Mage::getModel('shipping/rate_result');

        $result->append($this->_getDefaultRate());
        return $result;
    }
    public function getAllowedMethods()
    {
        return array($this->_code => $this->getConfigData('name'));
    }
    protected function _getDefaultRate()
    {
        $rate = Mage::getModel('shipping/rate_result_method');
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod($this->_code);
        $rate->setMethodTitle($this->getConfigData('title'));
        return $rate;
    }
}