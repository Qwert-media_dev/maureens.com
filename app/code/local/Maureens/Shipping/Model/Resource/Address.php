<?php

class Maureens_Shipping_Model_Resource_Address extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('maureens_shipping/address', 'id');
    }
}