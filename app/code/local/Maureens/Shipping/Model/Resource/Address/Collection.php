<?php

class Maureens_Shipping_Model_Resource_Address_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('maureens_shipping/address');
    }
}