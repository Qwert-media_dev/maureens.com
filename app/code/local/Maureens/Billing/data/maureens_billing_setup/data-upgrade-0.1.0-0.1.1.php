<?php
$content = <<<EOT
<p>We received your order #{{var increment_id}} .Thank you very much, your satisfaction is our top priority!</p>
<p>You can pay now and without any further effort you will receive your order to your shipping address.</p>
<p><span style="color: #00ccff;">We suggest to screenshot this page to save your order number and payment information.</span></p>
<ol style="padding-left: 40px;"><ol style="list-style-type: decimal;">
<li>Go to <strong>M-Pesa</strong> on your Phone</li>
<li>Select <strong>Lipa na M-Pesa</strong></li>
<li>Select <strong>Buy Goods and Services</strong></li>
<li>Enter our business no. <strong><span style="color: #ff0000;">756641</span></strong></li>
<li>Enter the payment amount of <strong>{{var payment_amount}}</strong></li>
<li>Enter <strong>your M-Pesa Pin</strong> and click<strong> OK</strong></li>
</ol></ol>
<p>&nbsp;</p>
<p>Make sure you send from the Mobile no. and with the registered M-Pesa name you gave us during the checkout process.</p>
<p>This way we can easily assign your payment to your order.</p>
<p>Your payment will be confirmed by text message and we start the delivery process. You will be contacted when your order arrives.</p>
EOT;

$cmsBlock = Mage::getModel('cms/block')->load('success_mpesa_mobile', 'identifier');
if ($cmsBlock->isObjectNew()) {
    $cmsBlock->setIdentifier('success_mpesa_mobile')
        ->setStores(array(0))
        ->setIsActive(true)
        ->setTitle('Success MPESA Send By Phone');
}
$cmsBlock->setContent($content)
    ->save();


$content = <<<EOT
<p>We received your order #{{var increment_id}} .Thank you very much, your satisfaction is our top priority!</p>
<p>You can pay now and without any further effort you will receive your order to your shipping address.</p>
<p><span style="color: #00ccff;">We suggest to screenshot this page to save your order number and payment information.</span></p>
<ol style="padding-left: 40px;"><ol style="list-style-type: decimal;">
<li>Go to your nearest <strong>M-Pesa</strong> Agent</li>
<li>
<p style="display: inline !important;">Tell the agent to send to our <strong>M-Pesa number</strong> <span style="color: #ff0000;"><strong>+254 718 555 332</strong></span></p>
</li>
<li>Tell the agent to send the payment amount of <span style="color: #ff0000;">&nbsp;<strong>{{var payment_amount}}</strong></span></li>
<li>Receive and <strong>keep your Payment Code</strong> (example: KK465VXYKS)</li>
<li>Contact us to <strong>give us the Payment Code</strong></li>
</ol></ol>
<p>&nbsp;</p>
<p>When we receive the Payment Code we can assign your payment to your order and start the delivery process.</p>
<p><span style="text-decoration: underline;">You can contact us here to give us your Payment Code:</span><br />WhatsApp, Viber, Text message or Call: +254 718 555 332.<br />Email: karibu@maureens.com</p>
<p>You will be contacted when your order arrives.</p>
EOT;

$cmsBlock = Mage::getModel('cms/block')->load('success_mpesa_agent', 'identifier');
if ($cmsBlock->isObjectNew()) {
    $cmsBlock->setIdentifier('success_mpesa_agent')
        ->setStores(array(0))
        ->setIsActive(true)
        ->setTitle('Success MPESA Send By Agent');
}
$cmsBlock->setContent($content)
    ->save();


$content = <<<EOT
<p>We received your order #{{var increment_id}} .Thank you very much, your satisfaction is our top priority!</p>
<p>You can pay now and without any further effort you will receive your order to your shipping address.</p>
<p><span style="color: #00ccff;">We suggest to screenshot this page to save your order number and payment information.</span></p>
<p>&nbsp;</p>
<p><span style="text-decoration: underline;"><span style="color: #000000; text-decoration: underline;">How to send your payment from MTN to M-Pesa:</span></span></p>
<ol style="padding-left: 40px;"><ol style="list-style-type: decimal;">
<li>Dial <strong>*830#</strong> to enter your Mobile wallet</li>
<li>Enter our <strong>M-Pesa number</strong> <span style="color: #ff0000;"><strong>+254 718 555 332</strong></span></li>
<li>Enter the payment amount of&nbsp;<span style="color: #ff0000;"><strong>{{var payment_amount}}</strong></span></li>
<li>You will receive a message with amount and forex exchange details.<strong> Press 1 to confirm</strong></li>
<li>You will receive a message prompting you to approve the debit/payment</li>
<li>Dial <strong>*182# &ndash; Pending Approvals &ndash; Approve amount by entering your MM Pin</strong></li>
</ol></ol>
<p>&nbsp;</p>
<p>Make sure you send from the Mobile no. and with the registered M-Pesa name you gave us during the checkout process.</p>
<p>This way we can easily assign your payment to your order.</p>
<p>Your payment will be confirmed by text message and we start the delivery process. You will be contacted when your order arrives.</p>
EOT;

$cmsBlock = Mage::getModel('cms/block')->load('success_mtn_mobile', 'identifier');
if ($cmsBlock->isObjectNew()) {
    $cmsBlock->setIdentifier('success_mtn_mobile')
        ->setStores(array(0))
        ->setIsActive(true)
        ->setTitle('Success MTN Mobile Money');
}
$cmsBlock->setContent($content)
    ->save();



?>