<?php

class Maureens_Billing_Model_Mtn extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'maureens_mtn';
    protected $_formBlockType = 'maureens_billing/checkout_form_mtn';
    protected $_infoBlockType = 'maureens_billing/checkout_info_mtn';

    public function assignData($data)
    {
        $info = $this->getInfoInstance();

        if ($data->getMaureensBillingInfo()) {
            $info->setMaureensBillingInfo($data->getMaureensBillingInfo());
        }

        return $this;
    }

    public function validate()
    {
        parent::validate();
        $info = $this->getInfoInstance();

        if (!$info->getMaureensBillingInfo() && Mage::getStoreConfig('payment/' . $this->_code . '/is_required')) {
            $errorCode = 'invalid_data';
            $errorMsg = $this->_getHelper()->__(Mage::getStoreConfig('payment/' . $this->_code . '/input_label'). " is a required field.\n");
        }

        if ($errorMsg) {
            Mage::throwException($errorMsg);
        }

        return $this;
    }


}