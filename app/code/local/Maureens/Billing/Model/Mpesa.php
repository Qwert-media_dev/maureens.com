<?php

class Maureens_Billing_Model_Mpesa extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'maureens_mpesa';
    protected $_formBlockType = 'maureens_billing/checkout_form_mpesa';
    protected $_infoBlockType = 'maureens_billing/checkout_info_mpesa';

    public function assignData($data)
    {
        $info = $this->getInfoInstance();

        if ($data->getMaureensBillingInfo()) {
            $info->setMaureensBillingInfo(serialize($data->getMaureensBillingInfo()));
        }

        return $this;
    }

    public function validate()
    {
        parent::validate();
        $info = $this->getInfoInstance();
        $billingInfo = unserialize($info->getMaureensBillingInfo());
        $radioList = $this->getRadioList();

        if ($radioList[$billingInfo['selected']]['is_required']) {
            if (isset($billingInfo['text']) && empty($billingInfo['text'])) {
                $errorCode = 'invalid_data';
                $errorMsg = $this->_getHelper()->__($radioList[$billingInfo['selected']]['input_label'] . " is a required field.\n");
            }
            if (isset($billingInfo['phone']) && empty($billingInfo['phone'])) {
                $errorCode = 'invalid_data';
                $errorMsg = $this->_getHelper()->__($radioList[$billingInfo['selected']]['input_label'] . " is a required field.\n");
            }
        }


        if ($errorMsg) {
            Mage::throwException($errorMsg);
        }

        return $this;
    }

    protected function getRadioList()
    {
        return unserialize(Mage::getStoreConfig('payment/' . $this->_code . '/radio_list'));
    }

}