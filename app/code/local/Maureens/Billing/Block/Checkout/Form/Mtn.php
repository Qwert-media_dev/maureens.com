<?php


class Maureens_Billing_Block_Checkout_Form_Mtn extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('maureens/billing/form/mtn.phtml');
    }

    public function getInputLabel()
    {
        return Mage::getStoreConfig('payment/' . $this->getMethodCode(). '/input_label');
    }

    /**
     * Retrieve field value data from payment info object
     *
     * @param   string $field
     * @return  mixed
     */
    public function getInfoData($field)
    {
        if ($this->getMethod()->getInfoInstance()->getData('method') == $this->getMethodCode())
            return $this->escapeHtml($this->getMethod()->getInfoInstance()->getData($field));
        return '';
    }
}