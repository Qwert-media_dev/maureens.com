<?php


class Maureens_Billing_Block_Checkout_Form_Mpesa extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('maureens/billing/form/mpesa.phtml');
    }

    public function getRadioOptions()
    {
        $radioList = Mage::getStoreConfig('payment/' . $this->getMethodCode() . '/radio_list');
        if ($radioList) {
            $radioList = unserialize($radioList);
            if (is_array($radioList)) {
                $radioOptionResults = array();
                foreach ($radioList as $key => $radioRow) {
                    $radioOptionResults[$radioRow['radio_label']][$key] = $radioRow;
                    $radioOptionResults[$radioRow['radio_label']]['method'] = $radioRow['method_type'];
                }
                return $radioOptionResults;
            }

//        $billingInfo = unserialize($this->getMethod()->getInfoInstance()->getData('maureens_billing_info'));

        }
    }
}