<?php


class Maureens_Billing_Block_Checkout_Info_Mpesa extends Mage_Payment_Block_Info
{
    protected function _prepareSpecificInformation($transport = null)
    {
        if (null !== $this->_paymentSpecificInformation) {
            return $this->_paymentSpecificInformation;
        }

        $data = array();
        if ($this->getInfo()->getMaureensBillingInfo()) {
            $billingInfo = unserialize($this->getInfo()->getMaureensBillingInfo());
            if (isset($billingInfo['hidden'])) {
                $data[Mage::helper('payment')->__($billingInfo['radio'])] = '';
            }
            if (isset($billingInfo['text'])) {
                $data[Mage::helper('payment')->__($billingInfo['radio'])] = $billingInfo['text'];
            }
            if (isset($billingInfo['phone'])) {
                $data[Mage::helper('payment')->__($billingInfo['radio'])] = $billingInfo['phone'];
            }
        }

        $transport = parent::_prepareSpecificInformation($transport);

        return $transport->setData(array_merge($data, $transport->getData()));
    }
}