<?php


class Maureens_Billing_Block_Checkout_Info_Mtn extends Mage_Payment_Block_Info
{
    protected function _prepareSpecificInformation($transport = null)
    {
        if (null !== $this->_paymentSpecificInformation) {
            return $this->_paymentSpecificInformation;
        }

        $data = array();
        if ($this->getInfo()->getMaureensBillingInfo()) {
            $data[Mage::helper('payment')->__(Mage::getStoreConfig('payment/' . $this->getInfo()->getMethod(). '/input_label'))] = $this->getInfo()->getMaureensBillingInfo();
        }

        $transport = parent::_prepareSpecificInformation($transport);

        return $transport->setData(array_merge($data, $transport->getData()));
    }
}