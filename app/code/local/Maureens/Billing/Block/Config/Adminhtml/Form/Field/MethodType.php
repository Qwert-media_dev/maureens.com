<?php

class Maureens_Billing_Block_Config_Adminhtml_Form_Field_MethodType
    extends Mage_Core_Block_Html_Select
{
    public function _toHtml()
    {
        $this->addOption('mobile','Mobile');
        $this->addOption('agent','Agent');
        $this->setExtraParams('style="width:80px"');
        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}