<?php

class Maureens_Billing_Block_Config_Adminhtml_Form_Field_Required
    extends Mage_Core_Block_Html_Select
{
    public function _toHtml()
    {
        $this->addOption('1','Yes');
        $this->addOption('0','No');
        $this->setExtraParams('style="width:80px"');
        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}