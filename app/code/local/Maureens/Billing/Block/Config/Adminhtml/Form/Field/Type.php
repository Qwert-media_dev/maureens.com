<?php

class Maureens_Billing_Block_Config_Adminhtml_Form_Field_Type
    extends Mage_Core_Block_Html_Select
{
    public function _toHtml()
    {
        $this->addOption('text','Text');
        $this->addOption('phone','Phone');
        $this->addOption('hidden','Hidden');
        $this->setExtraParams('style="width:80px"');
        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}