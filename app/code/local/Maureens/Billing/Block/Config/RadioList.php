<?php
class Maureens_Billing_Block_Config_RadioList
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    protected $_typeRenderer;
    protected $_requiredRenderer;
    protected $_methodTypeRenderer;

    public function _prepareToRender()
    {
        $this->addColumn('radio_label', array(
            'label' => Mage::helper('maureens_billing')->__('Radio Label'),
            'style' => 'width:200px',
        ));
        $this->addColumn('radio_type', array(
            'label' => Mage::helper('maureens_billing')->__('Input field type'),
            'renderer' => $this->_getTypeRenderer()
        ));
        $this->addColumn('method_type', array(
            'label' => Mage::helper('maureens_billing')->__('Method type'),
            'renderer' => $this->_getMethodTypeRenderer()
        ));
        $this->addColumn('input_label', array(
            'label' => Mage::helper('maureens_billing')->__('Input Label'),
            'style' => 'width:200px',
        ));
        $this->addColumn('is_required', array(
            'label' => Mage::helper('maureens_billing')->__('Required Field'),
            'style' => 'width:50px',
            'renderer' => $this->_getRequiredRenderer()
        ));
        $this->addColumn('input_placeholder', array(
            'label' => Mage::helper('maureens_billing')->__('Input Placeholder'),
            'style' => 'width:100px',
        ));
        $this->addColumn('input_value', array(
            'label' => Mage::helper('maureens_billing')->__('Input Value'),
            'style' => 'width:100px',
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('maureens_billing')->__('Add Radio');
    }
    protected function  _getTypeRenderer()
    {
        if (!$this->_typeRenderer) {
            $this->_typeRenderer = $this->getLayout()->createBlock(
                'maureens_billing/config_adminhtml_form_field_type', '',
                array('is_render_to_js_template' => true, 'element' => $this->getElement())
            );
        }
        return $this->_typeRenderer;
    }
    protected function  _getMethodTypeRenderer()
    {
        if (!$this->_methodTypeRenderer) {
            $this->_methodTypeRenderer = $this->getLayout()->createBlock(
                'maureens_billing/config_adminhtml_form_field_methodType', '',
                array('is_render_to_js_template' => true, 'element' => $this->getElement())
            );
        }
        return $this->_methodTypeRenderer;
    }
    protected function  _getRequiredRenderer()
    {
        if (!$this->_requiredRenderer) {
            $this->_requiredRenderer = $this->getLayout()->createBlock(
                'maureens_billing/config_adminhtml_form_field_required', '',
                array('is_render_to_js_template' => true, 'element' => $this->getElement())
            );
        }
        return $this->_requiredRenderer;
    }
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getTypeRenderer()
                ->calcOptionHash($row->getData('radio_type')),
            'selected="selected"'
        );
        $row->setData(
            'option_extra_attr_' . $this->_getRequiredRenderer()
                ->calcOptionHash($row->getData('is_required')),
            'selected="selected"'
        );
        $row->setData(
            'option_extra_attr_' . $this->_getMethodTypeRenderer()
                ->calcOptionHash($row->getData('is_required')),
            'selected="selected"'
        );
    }
}