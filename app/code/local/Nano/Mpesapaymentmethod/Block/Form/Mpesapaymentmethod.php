<?php
// app/code/local/Nano/Mpesapaymentmethod/Block/Form/Mpesapaymentmethod.php
class Nano_Mpesapaymentmethod_Block_Form_Mpesapaymentmethod extends Mage_Payment_Block_Form
{
  protected function _construct()
  {
    parent::_construct();
    $this->setTemplate('mpesapaymentmethod/form/mpesapaymentmethod.phtml');
  }
}