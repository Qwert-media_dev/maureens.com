<?php
// app/code/local/Nano/Mpesapaymentmethod/Block/Info/Mpesapaymentmethod.php
class Nano_Mpesapaymentmethod_Block_Info_Mpesapaymentmethod extends Mage_Payment_Block_Info
{
  protected function _prepareSpecificInformation($transport = null)
  {
    if (null !== $this->_paymentSpecificInformation)
    {
      return $this->_paymentSpecificInformation;
    }

    $data = array();
    if ($this->getInfo()->getMpesaName())
    {
      $data[Mage::helper('payment')->__('M-Pesa Name')] = $this->getInfo()->getMpesaName();
    }

    if ($this->getInfo()->getMpesaPhone())
    {
      $data[Mage::helper('payment')->__('M-Pesa Phone')] = $this->getInfo()->getMpesaPhone();
    }

    $transport = parent::_prepareSpecificInformation($transport);

    return $transport->setData(array_merge($data, $transport->getData()));
  }
}