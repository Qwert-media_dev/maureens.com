<?php
// app/code/local/Nano/Mpesapaymentmethod/Helper/Data.php
class Nano_Mpesapaymentmethod_Helper_Data extends Mage_Core_Helper_Abstract
{
  function getPaymentGatewayUrl()
  {
    return Mage::getUrl('mpesapaymentmethod/payment/gateway', array('_secure' => false));
  }
}