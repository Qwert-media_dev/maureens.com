<?php
// app/code/local/Nano/Mpesapaymentmethod/Model/Paymentmethod.php
class Nano_Mpesapaymentmethod_Model_Paymentmethod extends Mage_Payment_Model_Method_Abstract {
  protected $_code  = 'mpesapaymentmethod';
  protected $_formBlockType = 'mpesapaymentmethod/form_mpesapaymentmethod';
  protected $_infoBlockType = 'mpesapaymentmethod/info_mpesapaymentmethod';

  public function assignData($data)
  {
    $info = $this->getInfoInstance();

    if ($data->getMpesaName())
    {
      $info->setMpesaName($data->getMpesaName());
    }

    if ($data->getMpesaPhone())
    {
      $info->setMpesaPhone($data->getMpesaPhone());
    }

    return $this;
  }

  public function validate()
  {
    parent::validate();
    $info = $this->getInfoInstance();

    if (!$info->getMpesaName())
    {
      $errorCode = 'invalid_data';
      $errorMsg = $info->getMpesaName();
//      $errorMsg = $this->_getHelper()->__("MpesaName is a required field.\n");
    }

    if (!$info->getMpesaPhone())
    {
      $errorCode = 'invalid_data';
  //    $errorMsg .= $this->_getHelper()->__('MpesaPhone is a required field.');
    }

    if ($errorMsg)
    {
      Mage::throwException($errorMsg);
    }

    return $this;
  }

  public function getOrderPlaceRedirectUrl()
  {
	return Mage::getUrl('mpesapaymentmethod/payment/redirect', array('_secure' => false));
  }
}