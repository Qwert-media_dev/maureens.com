<?php
$installer = $this;
$installer->startSetup();
$installer->run("
ALTER TABLE `{$installer->getTable('sales/quote_payment')}`
ADD `nano_mpesa_name` VARCHAR( 255 ) NOT NULL,
ADD `nano_mpesa_phone` VARCHAR( 255 ) NOT NULL;

ALTER TABLE `{$installer->getTable('sales/order_payment')}`
ADD `nano_mpesa_name` VARCHAR( 255 ) NOT NULL,
ADD `nano_mpesa_phone` VARCHAR( 255 ) NOT NULL;
");
$installer->endSetup();