// for example 1:
function hideNil(){
    document.getElementById('nil').style.display = 'none';
    document.getElementById('nil').disabled = true;

}
function showNil(){
    document.getElementById('nil').value = '';
    document.getElementById('nil').style.display = 'block';
    document.getElementById('nil').disabled = false;

}
function showRadio(v){
    jQuery('.region_option').hide();
    jQuery('.region_option input').attr("disabled",true);
    jQuery('.letters select').attr("disabled",true);
    jQuery('.letters select').hide();
    hideNil();
    if(v != ''){
        jQuery('#'+ v).show();
        jQuery('#'+ v + ' input').attr("disabled",false);
        if (v.substring(v.length - 6, v.length) == '_other'){
            jQuery('#'+ v + ' select').attr("disabled", false);
            jQuery('#'+ v + ' select').show();
        }
    }
}

function showRadioLetters(element){
    jQuery('.region_option').hide();
    jQuery('.region_option input').attr("disabled",true);
    jQuery('.letters select').attr("disabled",true);
    hideNil();
    if(element.value != ''){
        jQuery('#'+ element.value).show();
        jQuery('#'+ element.value + ' input').attr("disabled",false);
        jQuery(element).attr("disabled", false);
    }
}


// for example 2:
function hideNil1(){
    document.getElementById('nil2').style.display = 'none';
}
function showNil1(){
    document.getElementById('nil2').value = '';
    document.getElementById('nil2').style.display = 'block';
}

function showInner(element) {
    jQuery('.maureens_shipping').hide();
    jQuery('.maureens_shipping').attr("disabled",true);
    jQuery(element).parent().children('.maureens_shipping').show();
    jQuery(element).parent().children('.maureens_shipping').attr("disabled",false);
}
