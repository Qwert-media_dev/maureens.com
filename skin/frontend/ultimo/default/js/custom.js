jQuery(function($){
	$(document).ready(function(){
/**
 * more / less buttons
**/
		// product quantity
		$('.lessQtyBefore').click(function(){
			var $qty = $(this).next('input');
			if($qty.val() > 1){$qty.val($qty.val() - 1);}
		});
		$('.moreQtyAfter').click(function(){
			var $qty = $(this).prev('input');
			$qty.val(parseInt($qty.val()) + 1);
		});

		// cart quantity
		$('.moreQtyBefore').click(function(){
			var $qty = $(this).next('input');
			$qty.val(parseInt($qty.val()) + 1);
		});
		$('.lessQtyAfter').click(function(){
			var $qty = $(this).prev('input');
			if($qty.val() > 0){$qty.val($qty.val() - 1);}
		});
/**
 *    product price
**/
		var delPriceDotZero = function delPriceDotZero(){
			$('.price:not(.done)').each(function(){
				$(this).html($(this).html().replace(/\.00\s*$/, '')).addClass('done');
			});
		}
		delPriceDotZero();
		var delPriceDotZeroInterval = setInterval(function(){
			delPriceDotZero();
		}, 300);

/**
 *    replace login width facebook
**/
		$('.singsys-fbconnect-login').insertAfter($('.page-title'));
	
	});
});